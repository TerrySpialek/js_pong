var canvas;
var context;

var pls;
var ball;
var id;

var lastTime;
var now;
var dt;

function Player(name, x, color) {
  this.name = name;
  this.x = x;
  this.y = 0;
  this.width=10;
  this.height=70;
  this.color = color;
  this.score =0;

  this.xSpeed=0;
  this.ySpeed=0;
}

function Ball(){
  this.x=0;
  this.y=500;
  this.width=20;
  this.height=20;

  this.xSpeed=2;
  this.ySpeed=2;
}

function collision(o1, o2){
  return (o1.x < o2.x + o2.width  && o1.x + o1.width  > o2.x &&
    o1.y < o2.y + o2.height && o1.y + o1.height > o2.y);
}

function bound(){
  for(i=0; i < pls.length; i++) {
    if(pls[i].y < 0) pls[i].y = 0;
    if( (pls[i].y + pls[i].height) > canvas.clientHeight)
      pls[i].y = canvas.clientHeight - pls[i].height;
  }
}

function resizeCanvas() {
  canvas.width = canvas.clientWidth;
  canvas.height = canvas.clientHeight;

  pls[1].x = canvas.clientWidth-10;
  screenReflash();
}

function reflashBall(){
  //buttom wall
  if(ball.y+10 >= canvas.clientHeight){
    console.log(ball.y+10 > canvas.clientHeigth);
    ball.y = canvas.clientHeight-10;
    ball.ySpeed *= -1;
  }
  //top wall
  if(ball.y <= 0){
    console.log(ball.y < 0);
    ball.y = 0;
    ball.ySpeed *= -1;
  }

  if(collision(ball, pls[0])){
    ball.ySpeed += pls[0].ySpeed;
    ball.xSpeed = ball.xSpeed == 2 ? -2 : 2;
  }
  if(collision(ball, pls[1])){
    ball.ySpeed += pls[1].ySpeed;
    ball.xSpeed = ball.xSpeed == 2 ? -2 : 2;
  }

  if(ball.x+10 > canvas.clientWidth){
    pls[0].score++;
    ball.xSpeed = -2;
  }
  if(ball.x < 0){
    pls[1].score++;
    ball.xSpeed = 2;
  }


  ball.x += ball.xSpeed;
  ball.y += ball.ySpeed;
}

function reflashPlayers(){
  for(i=0; i < pls.length; i++) {
    pls[i].x += pls[i].xSpeed
    pls[i].y += pls[i].ySpeed
  }
}

function screenReflash(){
  canvas.width = canvas.width;

  context.fillStyle="white";
  context.font = "100px Arial";
  context.textAlign = "start";
  context.textBaseline = "Middle";
  context.fillText(pls[0].score+":"+pls[1].score,canvas.width/2-50, 100);

  reflashPlayers();
  reflashBall();
  bound();
  for(i=0; i < pls.length; i++) {
    //console.log(pls[i].x+":"+pls[i].y);
    //context.rect(pls[i].x, pls[i].y, pls[i].width, pls[i].height);
    context.fillStyle=pls[i].color;
    context.fillRect(pls[i].x, pls[i].y, pls[i].width, pls[i].height);
  }

  context.fillStyle="red";
  context.fillRect(ball.x, ball.y, ball.width, ball.height);
}

function keyDown(e){
  //alert(e.keyCode);
  console.log('Down '+e.keyCode);
  switch(e.keyCode){
    case 38:
      pls[1].ySpeed = -5;
      break;
    case 40:
      pls[1].ySpeed = +5;
      break;
    case 87: //W
      pls[0].ySpeed = -5;
      break;
    case 83: //s
      pls[0].ySpeed = +5;
      break;
  }
}

function keyUp(e){
  //alert(e.keyCode);
  console.log('Up '+e.keyCode);

  switch(e.keyCode){
    case 38:
      pls[1].ySpeed = 0;
      break;
    case 40:
      pls[1].ySpeed = 0;
      break;
    case 87: //W
      pls[0].ySpeed = 0;
      break;
    case 83: //s
      pls[0].ySpeed = 0;
      break;

  }
}

function init() {
  window.requestAnimFrame = (function(){
    return  window.requestAnimationFrame       ||
	    window.webkitRequestAnimationFrame ||
	    window.mozRequestAnimationFrame    ||
	    function( callback ){
	      window.setTimeout(callback, 1000 / 60);
	    };
  })();

  document.addEventListener('keydown', keyDown);
  document.addEventListener('keyup', keyUp);

  window.addEventListener('resize', resizeCanvas, false);

  canvas = document.querySelector("#canvas");
  context = canvas.getContext("2d");

  pls = [new Player('1', 0, 'white'),
    new Player('1', canvas.clientWidth-10, 'green')];
  ball = new Ball();

  resizeCanvas();
}

function loop() {
  now = Date.now();
  dt  = (now - lastTime) / 1000.0;

  screenReflash();

  lastTime = now;
  requestAnimFrame(loop);
}

function main(){
  init();
  requestAnimFrame(loop);
}

main();
